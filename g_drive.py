"""

Sample metadata of all files & directories:
result = [
    {u'mimeType': u'text/plain', u'parents': [u'0ByrPcbC-4pd5SXI3cWZWMHg5eG8'], u'id': u'0ByrPcbC-4pd5MlhMQXdfdTUxd0k', u'name': u'NEWS.txt'},
    {u'mimeType': u'application/vnd.google-apps.folder', u'parents': [u'0ByrPcbC-4pd5VjhYUmh2NzVMOFk'], u'id': u'0ByrPcbC-4pd5SXI3cWZWMHg5eG8', u'name': u'Sub Folder'},
    {u'mimeType': u'text/x-chdr', u'parents': [u'0ByrPcbC-4pd5ampBUDBoWUVaSms'], u'id': u'0ByrPcbC-4pd5SHZEWUF2N0tYVlU', u'name': u'stdint.h'},
    {u'mimeType': u'application/vnd.google-apps.document', u'parents': [u'0ByrPcbC-4pd5ampBUDBoWUVaSms'], u'id': u'1F5Nna8kOvZmkw6-cpmI13Ohl4bvchW_sAKBEmkJeaYU', u'name': u'Download in sub folder'},
    {u'mimeType': u'application/vnd.google-apps.folder', u'parents': [u'0ACrPcbC-4pd5Uk9PVA'], u'id': u'0ByrPcbC-4pd5ampBUDBoWUVaSms', u'name': u'Sunday Folder'},
    {u'mimeType': u'application/vnd.google-apps.document', u'parents': [u'0ACrPcbC-4pd5Uk9PVA'], u'id': u'1zNDAR-QXY8CJiCp6LhLHXwCeauEyF4yOTpzjgSdL7BA', u'name': u'Sunday 18 September 2016'},
    {u'mimeType': u'image/jpeg', u'parents': [u'0ACrPcbC-4pd5Uk9PVA'], u'id': u'0ByrPcbC-4pd5YjN0WHlYY0plWHM', u'name': u'image1.JPG'},
    {u'mimeType': u'image/jpeg', u'parents': [u'0ACrPcbC-4pd5Uk9PVA'], u'id': u'0ByrPcbC-4pd5ejZSU1c0TEZhbkU', u'name': u'image3.JPG'},
    {u'mimeType': u'image/jpeg', u'parents': [u'0ACrPcbC-4pd5Uk9PVA'], u'id': u'0ByrPcbC-4pd5aFpBdEI2cGpnbzg', u'name': u'image2.JPG'},
    {u'mimeType': u'application/x-msdownload', u'parents': [u'0ByrPcbC-4pd5VjhYUmh2NzVMOFk'], u'id': u'0ByrPcbC-4pd5M1RZMXVJSE5FOFU', u'name': u'PUTTY.EXE'},
    {u'mimeType': u'image/jpeg', u'parents': [u'0ByrPcbC-4pd5VjhYUmh2NzVMOFk'], u'id': u'0ByrPcbC-4pd5WlFETFFCUTYtRFU', u'name': u'1.jpg'},
    {u'mimeType': u'application/vnd.google-apps.folder', u'parents': [u'0ACrPcbC-4pd5Uk9PVA'], u'id': u'0ByrPcbC-4pd5VjhYUmh2NzVMOFk', u'name': u'New Folder'},
]

"""
import os
import ssl
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import *
from pyvirtualdisplay import Display

import httplib2
import io
from googleapiclient.http import MediaIoBaseDownload
from googleapiclient import discovery
from oauth2client import client
from base import Base

if os.name != 'nt':
    display = Display(visible=0, size=(800, 600))
    display.start()

cur_dir = os.path.dirname(os.path.realpath(__file__))
if cur_dir[-1] != '/':
    cur_dir += '/'


class GDriveDownloader(Base):

    drive_service = None
    all_metadata = None

    def __init__(self):
        Base.__init__(self)

    def connect(self):
        if self.debug:
            self.all_metadata = dict()
            self.all_metadata = [
                {
                    'mimeType': 'image/jpeg',
                    'parents': ['0ByrPcbC-4pd5VjhYUmh2NzVMOFk'],
                    'id': '0ByrPcbC-4pd5WlFETFFCUTYtRFU',
                    'name': '1.jpg',
                },
                {
                    'mimeType': 'application/vnd.google-apps.folder',
                    'parents': ['0ACrPcbC-4pd5Uk9PVA'],
                    'id': '0ByrPcbC-4pd5VjhYUmh2NzVMOFk',
                    'name': 'New Folder',
                },
                {
                    'mimeType': 'application/vnd.google-apps.spreadsheet',
                    'parents': ['0ACrPcbC-4pd5Uk9PVA'],
                    'id': '1sJKIGhjvdoq3sWn8p95Ynm6xNg62VGWhsOxnVmHBQI8',
                    'name': 'TEST Sheet',
                },
                {
                    'mimeType': 'application/vnd.google-apps.document',
                    'parents': ['0ACrPcbC-4pd5Uk9PVA'],
                    'id': '1v77AKFgqvJKlKpvB6edW4fEHik4-hCG7G8VJK6XA_Ew',
                    'name': 'TEST Document',
                },
                {
                    'mimeType': 'application/pdf',
                    'parents': ['0ACrPcbC-4pd5Uk9PVA'],
                    'id': '0ByrPcbC-4pd5c3RhcnRlcl9maWxl',
                    'name': 'Getting started',
                }
            ]

            return True

        flow = client.flow_from_clientsecrets(
            self.get_param_from_xml('GDRIVE_CREDENTIAL_PATH'),
            scope='https://www.googleapis.com/auth/drive.readonly',
            redirect_uri='urn:ietf:wg:oauth:2.0:oob')

        print 'Connecting to Google Drive...'
        s_time = time.time()

        auth_uri = flow.step1_get_authorize_url()

        driver = webdriver.Firefox()
        try:
            driver.get(auth_uri)
            try:
                elem = driver.find_element_by_name("Email")
                b_logged = False
            except NoSuchElementException:
                b_logged = True

            if not b_logged:
                # type user's email
                elem = driver.find_element_by_name("Email")
                elem.clear()
                elem.send_keys(self.get_param_from_xml('GDRIVE_EMAIL'))
                btn_elem = driver.find_element_by_name('signIn')
                btn_elem.send_keys(Keys.RETURN)

                while True:
                    try:
                        pwd_elem = driver.find_element_by_id('Passwd')
                        break
                    except NoSuchElementException:
                        time.sleep(1)
                # input user's password
                pwd_elem = driver.find_element_by_id('Passwd')
                pwd_elem.clear()
                pwd_elem.send_keys(self.get_param_from_xml('GDRIVE_PWD'))
                btn_elem = driver.find_element_by_id('signIn')
                btn_elem.send_keys(Keys.RETURN)

                # Check login status
                s_time = time.time()
                while True:
                    try:
                        btn_elem = driver.find_element_by_name('signIn')
                        print "Failed to login, wrong password"
                        driver.close()
                        return False
                    except NoSuchElementException:
                        pass

                    try:
                        btn_elem = driver.find_element_by_id('submit_approve_access')
                        break
                    except NoSuchElementException:
                        pass

                    if time.time() - s_time > 30:
                        print "Failed to login, time out"
                        driver.close()
                        return False
                    else:
                        time.sleep(1)

            # Tap "Allow" button
            btn_elem = driver.find_element_by_id('submit_approve_access')
            # Wait for submit button to be enabled
            while True:
                try:
                    btn_elem.send_keys(Keys.RETURN)
                    break
                except InvalidElementStateException:
                    time.sleep(1)
            # Get Auth code
            while True:
                try:
                    code_elem = driver.find_element_by_id('code')
                    break
                except NoSuchElementException:
                    time.sleep(1)

            code_elem = driver.find_element_by_id('code')
            auth_code = code_elem.get_attribute('value')

            driver.close()

        except TimeoutException:
            print "Loading took too much time!"
            driver.close()
            return False

        credentials = flow.step2_exchange(auth_code)
        http_auth = credentials.authorize(httplib2.Http())
        self.drive_service = discovery.build('drive', 'v3', http_auth)
        print "Successfully connected to the Google Drive Server."
        print "Elapsed: ", time.time() - s_time
        try:
            self.all_metadata = self.drive_service.files().list(fields='files(id, name, parents, mimeType)').execute()
            self.all_metadata = self.all_metadata['files']
            return True
        except ssl.SSLEOFError:
            print "Failed to get file list, please try again later."
            return False

    def download_all(self):
        if self.drive_service is None:
            print "Failed to get service, please connect again..."
            return False
        for f in self.all_metadata:
            print f['name']
            # download_file(f['id'], f['mimeType'], f['name'])

    def download_file(self, file_id, mimeType, filename):
        if "google-apps" in mimeType:
            # skip google files
            return
        if os.path.isfile(filename):
            print "File already exists, skipping.."
            return True

        print "Downloading ", filename
        request = self.drive_service.files().get_media(fileId=file_id)
        fh = io.FileIO(filename, 'wb')
        downloader = MediaIoBaseDownload(fh, request)
        done = False
        while done is False:
            status, done = downloader.next_chunk()
            print "Download %d%%." % int(status.progress() * 100)

    def download_item(self, item_id, item_mime, dest_path):
        """
        Download item from Google Drive.
        - If current item is file, just download it with 'download_file' function.
        - Otherwise, get all child items and apply this function again for recursive call
        :param item_id: id of current id
        :param item_mime: mimeType
        :param dest_path: destination path
        :return:
        """
        print '-- Downloading -- '
        print 'id: ', item_id, 'path: ', dest_path
        info = self.get_item_by_id(item_id)
        print "Detailed info: ", info
        if info is None:
            print "Cannot get information from its id, exiting..."
            return False
        # In case of file
        if 'vnd.google-apps.folder' not in item_mime:
            self.download_file(item_id, item_mime, dest_path)
        else:   # Folder
            # Before downloading, create folder
            self.create_dir(dest_path)
            # get child items
            child_list = []
            for child in self.all_metadata:
                try:
                    if item_id in child['parents']:
                        child_list.append(child)
                except KeyError:
                    continue

            for child in child_list:
                self.download_item(child['id'], child['mimeType'], dest_path + '/' + child['name'])

    def get_child_list(self, folder_id):
        """
        Get children list of given folder
        :param folder_id: ID of folder
        :return: List of metadata(JSON)
        """
        if self.all_metadata is None:
            print "Not connected yet, please connect first and try again."

        elif folder_id in ['', '/'] or folder_id is None:
            # Get files & folders in the root folder.
            # These items' parents is None.
            l_root = []
            for t in self.all_metadata:
                try:
                    if self.get_item_by_id(t['parents'][0]) is None:
                        l_root.append(t)
                except KeyError:
                    continue
            return l_root
        else:
            l_result = []
            for meta in self.all_metadata:
                try:
                    if folder_id in meta['parents']:
                        l_result.append(meta)
                except KeyError:
                    continue
            return l_result

    def get_full_path(self, _id):
        """
        Get full path of given item
        :param _id:
        :return:
        """
        cur_item = self.get_item_by_id(_id)
        if cur_item is None:
            return ''

        path = cur_item['name']
        try:
            parent_item = self.get_item_by_id(cur_item['parents'][0])
            while parent_item is not None:
                path = parent_item['name'] + '/' + path
                parent_item = self.get_item_by_id(parent_item['parents'][0])
        except KeyError:
            pass
        return path

    def get_item_by_id(self, _id):
        """
        :param _id:
        :return:
        """
        cur_item = None
        for it in self.all_metadata:
            if it['id'] == _id:
                cur_item = it

        return cur_item

    def get_parent_id_list(self, _id):
        """
        Get ID list of parent items.
        :param _id:
        :return:
        """
        cur_item = self.get_item_by_id(_id)
        if cur_item is None:
            return []

        l_parents = []
        try:
            parent_item = self.get_item_by_id(cur_item['parents'][0])
            while parent_item is not None:
                l_parents.append({'id': parent_item['id'], 'name': parent_item['name']})
                parent_item = self.get_item_by_id(parent_item['parents'][0])
        except KeyError:
            pass

        if len(l_parents) > 0:
            l_parents.reverse()

        return l_parents

    @staticmethod
    def create_dir(path):
        if not os.path.exists(path):
            os.makedirs(path)


if __name__ == '__main__':
    s_time = time.time()
    g = GDriveDownloader()

    g.connect()

    g.download_all()

    print "--"
    print g.get_child_list('0ByrPcbC-4pd5VjhYUmh2NzVMOFk')

    print " -- Getting all metadata..."
    for item in g.all_metadata:
        print item
    # print g.all_metadata

    print "Elapsed: ", time.time() - s_time



