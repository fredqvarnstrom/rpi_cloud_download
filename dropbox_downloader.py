import os
import time

from base import Base
import dropbox


# DROPBOX_TOKEN = 'nwB7S7QKf0AAAAAAAAAADtLfxVbw7IgY4inT1nfvV2S_vbiJ1ttyxxqvLrTgCFa0'

class DropBoxDownloader(Base):

    client = None

    def __init__(self):
        Base.__init__(self)

    def connect(self):
        self.client = dropbox.client.DropboxClient(self.get_param_from_xml('DROPBOX_TOKEN'))
        try:
            account_info = self.client.account_info()
            print 'Linked Account: ', account_info
            return True

        except dropbox.rest.ErrorResponse as e:
            print e
            return False

    def get_root_dir(self):
        return self.client.metadata('/')

    def get_metadata(self, path):
        return self.client.metadata(path)

    def download_file(self, metadata, root_dir=None):
        """
        Download file from dropbox
        :param metadata: starts with "/"
        :param root_dir: destination file name
        :return:
        """
        f, metadata = self.client.get_file_and_metadata(metadata)
        print 'Downloading ', metadata['path']
        if root_dir is not None:
            out = open(root_dir + metadata['path'], 'wb')
        else:
            out = open(metadata['path'], 'wb')

        out.write(f.read())
        out.close()

    def get_child(self, metadata):
        """
        Get children items.
        """
        # TODO: Implement here...
        cur_meta = self.client.metadata(metadata)
        child_list = cur_meta['contents']

        return child_list

    def download_directory(self, path, base_dir):
        """
        Download all child items to base_dir
        """
        create_dir(base_dir + path)
        child_list = self.get_child(path)
        for child in child_list:
            if child['is_dir']:
                self.download_directory(child['path'], base_dir)
            else:
                self.download_file(child['path'], base_dir)


def create_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)


if __name__ == '__main__':
    # f = open('config.xml', 'rb')
    # response = client.put_file('/magnum-opus.txt', f)
    # print 'uploaded: ', response
    s_time = time.time()
    d = DropBoxDownloader()

    d.connect()

    root = d.get_child('/static')
    for item in root:
        print item

    d.download_directory('/static', 'downloaded')

    print "Elapsed: ", time.time() - s_time

