## Working with Google Drive

*Preference:*

- https://developers.google.com/drive/v3/web/quickstart/python
- http://www.iperiusbackup.net/en/how-to-enable-google-drive-api-and-get-client-credentials/
            
*Required:* 
    
- Google account information (email, password)
- credential file (JSON type)
    
    username: cloudback2016@gmail.com
    
    pwd: back2016
    
### Install dependencies

        sudo apt-get update
        
        sudo pip install google-api-python-client
        
        sudo apt-get install python-pip iceweasel xvfb
        sudo pip install pyvirtualdisplay selenium


## Working with DropBox
    
*Preference:*

- http://stackoverflow.com/questions/23894221/upload-file-to-my-dropbox-from-python-script

*Required:* 

- Token value for each account.
- No need dropbox account info.

### Install dependencies
        
        sudo pip install dropbox

## Working with OneDrive

*Preference:*

- https://dev.onedrive.com/app-registration.htm
- https://github.com/onedrive/onedrive-sdk-python


## Preparing web server
    
### Install dependencies
    
    sudo pip install flask
    
    sudo chmod -R 777 ~/rpi_cloud_download
    cd ~/rpi_cloud_download
    sudo python flask_rpi_cloud_download.py